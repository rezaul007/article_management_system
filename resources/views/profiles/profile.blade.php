{{--My profile--}}
{{--{{$user->name}}--}}
{{--{{$user->id}}--}}
{{--{{$user->email}}--}}


{{--{{$user->profile->last_name}}--}}

@extends('layout.default')

@section('main_content')

<div class="container">
    <div class="row">
        <div >
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 lead">{{$user->name}}'s profile<hr></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img class="img-circle avatar avatar-original" style="-webkit-user-select:none; width: 250px; height: 250px;
              display:block; margin:auto;" src="{!! asset($user->profile->profile_picture) !!}">

                            {{--<div><img src="{!! asset($profile->profile_picture) !!}" style="width: 150px; height: 150px"></div>--}}
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="only-bottom-margin">{{$user->profile->first_name}}&nbsp;{{$user->profile->last_name}}</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="text-muted">Email:</span>{{$user->email}}<br>
                                    <span class="text-muted">Birth date:</span> {{$user->profile->birthday}}<br>
                                    <span class="text-muted">Gender:</span> {{$user->profile->gender}}<br>
                                    <span class="text-muted">Personal phone:</span> {{$user->profile->personal_phone}}<br>
                                    <span class="text-muted">Home Phone:</span> {{$user->profile->home_phone}}<br>
                                    <span class="text-muted">Office Phone:</span> {{$user->profile->office_phone}}<br>
                                    <span class="text-muted">Current Address:</span> {{$user->profile->current_address}}<br>
                                    <span class="text-muted">Permanent Address:</span> {{$user->profile->permanent_address}}<br>

                                    <small class="text-muted">Created: {{$user->profile->created_at}}</small>
                                </div>
                                <div class="col-md-6">
                                    <div class="activity-mini">
                                        <i class="glyphicon glyphicon-comment text-muted"></i> 500
                                    </div>
                                    <div class="activity-mini">
                                        <i class="glyphicon glyphicon-thumbs-up text-muted"></i> 1500
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            {{--<button class="btn btn-default pull-right"><i class="glyphicon glyphicon-pencil"></i> Edit</button>--}}
                    <a class="btn btn-default pull-right" href="{!! url('profiles/'.$user->profile->id.'/edit') !!}">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

