{{--@foreach($profiles as $profile)--}}

{{--{{$profile['email']}}--}}
{{--{{$profile['profile']['personal_phone']}}--}}
{{--{{$profile['profile']['last_name']}}--}}
{{--@endforeach--}}

@extends('layout.default')

@section('main_content')

<div class="container">
    <div class="row">
        <div >
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 lead">{{$profiles->user->name}}'s profiles<hr></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img class="img-circle avatar avatar-original" style="-webkit-user-select:none; width: 250px; height: 250px;
              display:block; margin:auto;" src="{!! asset($profiles->profile_picture) !!}">
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="only-bottom-margin">{{$profiles->first_name}}&nbsp;{{$profiles->last_name}}</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="text-muted">Email:</span> {{$profiles->user->email}} <br>
                                    <span class="text-muted">Birth date:</span> {{$profiles->birthday}}<br>
                                    <span class="text-muted">Gender:</span> {{$profiles->gender}}<br>
                                    <span class="text-muted">Personal phone:</span> {{$profiles->personal_phone}}<br>
                                    <span class="text-muted">Home Phone:</span> {{$profiles->home_phone}}<br>
                                    <span class="text-muted">Office Phone:</span> {{$profiles->office_phone}}<br>
                                    <span class="text-muted">Current Address:</span> {{$profiles->current_address}}<br>
                                    <span class="text-muted">Permanent Address:</span> {{$profiles->permanent_address}}<br>

                                    <small class="text-muted">Created: {{$profiles->created_at}}</small>
                                </div>
                                <div class="col-md-6">
                                    <div class="activity-mini">
                                        <i class="glyphicon glyphicon-comment text-muted"></i> 500
                                    </div>
                                    <div class="activity-mini">
                                        <i class="glyphicon glyphicon-thumbs-up text-muted"></i> 1500
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            {{--<button class="btn btn-default pull-right"><i class="glyphicon glyphicon-pencil"></i> Edit</button>--}}
                    {{--<a href="{!! url('profiles/'.$profiles->id.'/edit') !!}">Edit</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

