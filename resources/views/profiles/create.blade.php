@extends('layout.default')

@section('main_content')

<h1>Profile</h1>

<hr>
@include ('errors.list')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
{!! Form::open(['url' => 'profiles', 'files'=> true]) !!}
{!! Form::hidden('user_id',Auth::user()->id) !!}
{{--{{ Auth::user()->id }}--}}
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('personal_phone', 'Personal Phone:') !!}
    {!! Form::text('personal_phone', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('home_phone', 'Home Phone:') !!}
    {!! Form::text('home_phone', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('office_phone', 'Office Phone:') !!}
    {!! Form::text('office_phone', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('current_address', 'Current Address:') !!}
    {!! Form::textarea('current_address', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('permanent_address', 'Permanent Address:') !!}
    {!! Form::textarea('permanent_address', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('birthday', 'Date of Birth:') !!}
    {!! Form::input('date', 'birthday', null,['class'=>'form-control']) !!}
</div>
<div class="form-group"> SEX:
{{ Form::radio('gender', 'male') }} Male
{{ Form::radio('gender', 'female', true) }}Female
</div>

<div class="form-group">
    {!! Form::label('img_path', 'Image') !!}
    {!! Form::file('image') !!}
</div>

<div class="form-group">
    {!! Form::submit('Update Profile', ['class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}

@stop
