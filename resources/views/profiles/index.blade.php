@extends('layout.default')

@section('main_content')

<h1>Profiles</h1>

<hr>
    @foreach($profiles as $profile)
    <article>
        <tr>
        <td>
        <h2>{{$profile->first_name}}&nbsp;{{$profile->last_name}}</h2>
            <div><img src="{!! asset($profile->profile_picture) !!}" style="width: 150px; height: 150px"></div>
        </td>
        <td>
            <a href="{!! url('profiles/'.$profile->id.'/edit') !!}">Edit</a> |
            <a href="{!! url('profiles/'.$profile->id) !!}">View</a> |
            {!! Form::open(array('method'=>'DELETE','route'=>array('profiles.destroy',$profile->id))) !!}
            {!! Form::submit('delete') !!}
            {!! Form::close() !!}</a>
        </td>
        </tr>
        <hr>
    </article>
    @endforeach
@stop

