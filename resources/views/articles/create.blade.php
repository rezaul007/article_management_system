@extends('layout.default')

@section('main_content')

<h1>Articles Write Here</h1>

<hr>
@include ('errors.list')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
{!! Form::open(['url' => 'articles','files'=> true]) !!}
{!! Form::hidden('user_id', Auth::user()->id )!!}
<div class="form-group">
    {!! Form::label('article_title', 'Title:') !!}
    {!! Form::text('article_title', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('article_detail', 'Details:') !!}
    {!! Form::textarea('article_detail', null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
{!! Form::label('Select A Forum:') !!}<br />
{{ Form::select('forum_id', $forums ) }}

</div>

<div class="form-group">
    {!! Form::label('img_path', 'Image') !!}
    {!! Form::file('image') !!}
</div>
<div class="form-group">
    {!! Form::label('img_caption', 'Image Caption') !!}
    {!! Form::text('img_caption', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group">
    {!! Form::submit('Add Article', ['class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}



@stop

@section('js')
    <script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('article_detail'),{
            uiColor: '#AADC6E'
        };
    </script>
    {{--<script>CKEDITOR.replace('html_summary');</script>--}}
@endsection