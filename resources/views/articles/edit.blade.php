@extends('Layout.default')
@section('main_content')

    <h1>Article Update</h1>
{!! Form::model($articles,['method'=>'PATCH', 'route'=>['articles.update',$articles->id]]) !!}
    <div class="form-group">
    {{--<label>Title</label><br>--}}
        {!! Form::label( 'Title:') !!}
        {!! Form::text('article_title',null, ['class'=>'form-control']) !!}
        </div>
    <div class="form-group">
    {{--<label>Details</label><br>--}}
        {!! Form::label( 'Details:') !!}
        {!! Form::textarea('article_detail',null,['class'=>'form-control']) !!}
        </div>
{{--<br><br>--}}
{{--<button type="submit">update</button>--}}

    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
    </div>
@stop