@extends('layout.default')

@section('main_content')

<h1>Articles</h1>
<a class="btn btn-primary" href="{!! url('articles\create') !!}" role="button">Add Article</a>
<hr>
    @foreach($articles as $article)



    <article>
        <tr>
            <div class="col-sm-2"><img src="{!! asset($article->img_path) !!}" style="width: 150px; height: 150px"></div>
            <div class="col-sm-10">
            <td>
        <h2>{!! $article->article_title !!}</h2>
        <div class="body">{!! $article->article_detail !!}</div>
                <dd>Forum Title:  {!! $article->forum->forum_title !!}</dd>
                <dd>Post By:  {!! $article->user->name !!}</dd>
                <dd>Created at:  {!! $article->created_at !!}</dd>
                <dd>Updated at:  {!! $article->updated_at !!}</dd>
            </td>
            </div>

        <td><br>

            {{--<button type="button" class="btn btn-secondary"><a href="{!! url('articles/'.$article->id.'/edit') !!}">Edit</a></button>--}}
{{--            <a href="{!! url('articles/'.$article->id.'/edit') !!}">Edit</a> |--}}
            <a class="btn btn-primary" href="{!! url('articles/'.$article->id.'/edit') !!}" role="button">Edit</a>
            <a class="btn btn-primary" href="{!! url('articles/'.$article->id) !!}" role="button" >View</a>
            {{--<style="position:relative;></style>--}}
            {!! Form::open(array('method'=>'DELETE','route'=>array('articles.destroy',$article->id))) !!}
            {!! Form::submit ('delete',array('class'=>'btn btn-primary', 'role'=>'button', 'float'=>'right'))!!}
            {!! Form::close() !!}
            {{--{!! Form::open(array('method'=>'DELETE','route'=>array('articles.destroy',$article->id))) !!}--}}
            {{--{!! Form::submit ('delete')!!}--}}
            {{--{!! Form::close() !!}--}}
        </td>

        <hr>
        </tr>
    </article>
    @endforeach
@stop