@extends('layout.default')

@section('main_content')

<h1>Forum Write Here</h1>

<hr>
@include ('errors.list')
{!! Form::open(['url' => 'forums']) !!}
<div class="form-group">
    {!! Form::label('forum_title', 'Title:') !!}
    {!! Form::text('forum_title', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('forum_detail', 'Details:') !!}
    {!! Form::textarea('forum_detail', null,['class'=>'form-control']) !!}
</div>



<div class="form-group">
    {!! Form::submit('Add Forum', ['class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}



@stop

@section('js')
    <script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('forum_detail'),{
            uiColor: '#AADC6E'
        };
    </script>
    {{--<script>CKEDITOR.replace('html_summary');</script>--}}
@endsection