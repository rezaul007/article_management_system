@extends('layout.default')

@section('main_content')

    <h1>forums</h1>
    <a class="btn btn-primary" href="{!! url('forums\create') !!}" role="button">Add Forum</a>
    <hr>
    @foreach($forums as $forum)

        <forum>
            <tr>
                <td>
                    <h2>{!! $forum->forum_title !!}</h2>
                    <div class="body">{!! $forum->forum_detail !!}</div>
                </td>
                <td>
                    <a class="btn btn-primary" href="{!! url('forums/'.$forum->id.'/edit') !!}" role="button">Edit</a>
                    <a class="btn btn-primary" href="{!! url('forums/'.$forum->id) !!}" role="button">View</a style="position:relative;>
                    {!! Form::open(array('method'=>'DELETE','route'=>array('forums.destroy',$forum->id))) !!}
                    {!! Form::submit('delete',array('class'=>'btn btn-primary', 'role'=>'button', 'float'=>'right')) !!}
                    {!! Form::close() !!}

            {{--{!! Form::open(array('method'=>'DELETE','route'=>array('articles.destroy',$article->id))) !!}--}}
            {{--{!! Form::submit ('delete',array('class'=>'btn btn-primary', 'role'=>'button', 'float'=>'right'))!!}--}}
            {{--{!! Form::close() !!}--}}
                </td>

                <hr>
            </tr>
        </forum>
    @endforeach
@stop