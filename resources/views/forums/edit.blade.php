@extends('Layout.default')
@section('main_content')

    <h1>Forum Update</h1>
{!! Form::model($forums,['method'=>'PATCH', 'route'=>['forums.update',$forums->id]]) !!}
    <div class="form-group">
    {{--<label>Title</label><br>--}}
        {!! Form::label( 'Title:') !!}
        {!! Form::text('forum_title',null, ['class'=>'form-control']) !!}
        </div>
    <div class="form-group">
    {{--<label>Details</label><br>--}}
        {!! Form::label( 'Details:') !!}
        {!! Form::textarea('forum_detail',null,['class'=>'form-control']) !!}
        </div>
{{--<br><br>--}}
{{--<button type="submit">update</button>--}}

    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
    </div>
@stop