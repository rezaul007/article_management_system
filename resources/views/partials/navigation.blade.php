<div class="header">

    <div class="header-nav">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><span>Q</span>uickly</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    {{--<li class="hvr-bounce-to-bottom active"><a href="/ams2/public/">Home</a></li>--}}
                    <li class="hvr-bounce-to-bottom "><a href="{{ url('/') }}">Home</a></li>
                    <li class="hvr-bounce-to-bottom "><a href="{{ url('/forums') }}">Forum</a></li>
                    <li class="hvr-bounce-to-bottom "><a href="{{ url('/articles') }}">Article</a></li>
                    {{--<li class="hvr-bounce-to-bottom"><a href="">About</a></li>--}}
                    {{--<li class="hvr-bounce-to-bottom"><a href="">Portfolio</a></li>--}}
                    {{--<li class="hvr-bounce-to-bottom"><a href="">Pages</a></li>--}}
                    <li class="hvr-bounce-to-bottom "><a href="contact">Contact Us</a></li>
                </ul>
                <div class="sign-in">
                    <ul>
                        {{--<li><a href="{{ url('/login') }}">Login</a>/</li>--}}
                        {{--<li><a href="{{ url('/register') }}">Register</a></li>--}}
<ul>
                        {{--<ul class="nav navbar-nav navbar-right">--}}
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                                <li><a href="{{ url('/login') }}">Login</a></li> |
                                <li><a href="{{ url('/register') }}">Register</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position:relative; padding-left:50px;">
                                        {{--<img src="{!! asset('uploads/avators/'.Auth::user()->avator) !!}" style="width:32px; height:32px; position:absolute; top:10px; left:10px; border-radius:50% ">--}}
                                        <img style="width:32px; height:32px; position:absolute; left:10px; border-radius:30% " src="{!! asset(Auth::user()->profile->profile_picture) !!}">
                                        {{ Auth::user()->name }} <span class="caret"></span>


                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/myprofile') }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                                        <li><a href="{{ url('/mypost') }}"><i class="fa fa-btn fa-user"></i>My Post</a></li>
                                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>

                                    </ul>
                                </li>
                            @endif
                        </ul>

                    </ul>
                </div>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>