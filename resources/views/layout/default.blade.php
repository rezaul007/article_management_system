<!DOCTYPE html>
<html>

<!-- Mirrored from p.w3layouts.com/demos/quickly/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jun 2016 06:50:53 GMT -->
<head>
    <title>Quickly a Blogging Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Quickly Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="{!! asset('css/bootstrap.css')!!}" rel="stylesheet" type="text/css" media="all" />
    {{--<link href="{!! asset('css/bootstrap.min.css')!!}" rel="stylesheet">--}}
    <link href="{!! asset('css/style.css')!!}" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.html"></script>
    <script type="text/javascript" src="js/easing.html"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
</head>

<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-30027142-1', 'w3layouts.com');
    ga('send', 'pageview');
</script>
<script async type='text/javascript' src='../../../../cdn.fancybar.net/ac/fancybar6a2f.js?zoneid=1502&amp;serve=C6ADVKE&amp;placement=w3layouts' id='_fancybar_js'></script>


<!-- banner-body -->
<div class="banner-body">
    <div class="container">
        <!-- header -->


        @include('partials.navigation')


            <!-- search-scripts -->
            <script src="js/classie.html"></script>
            <script src="js/uisearch.html"></script>
            <script>
                new UISearch( document.getElementById( 'sb-search' ) );
            </script>
            <!-- //search-scripts -->

        <!-- //header -->
{{--@include('partials.middle')--}}
        {{--@yield('allforum')--}}
    @yield('main_content')

    {{--<div>--}}
            {{--<h1>Hello World</h1>--}}
        {{--</div>--}}
    </div>

</div>
<!-- //banner-body -->
<!-- footer -->


@include('partials.footer')


        <!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"> </script>
<!-- //for bootstrap working -->
</body>

<!-- Mirrored from p.w3layouts.com/demos/quickly/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jun 2016 06:52:10 GMT -->
</html>
@yield('js')
