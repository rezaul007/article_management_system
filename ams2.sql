-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 15, 2016 at 02:45 PM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams2`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `forum_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `article_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `img_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `forum_id`, `user_id`, `article_title`, `article_detail`, `img_path`, `img_caption`, `extension`, `created_at`, `updated_at`) VALUES
(13, 1, 1, 'Title1', 'Title1 kshfdldsgldfj;lgdklf\r\n', 'Article/25march26.jpg', '', '', '2016-06-13 23:17:28', '2016-06-14 01:31:31'),
(14, 1, 2, 'Kamals 1st post', '<p>as,kfsldg/fs/bg;</p>\r\n', 'Article/26marchGoogle.jpg', '', '', '2016-06-14 00:25:16', '2016-06-14 00:25:16'),
(16, 2, 4, '‘রোনালদো উল্টোপাল্টা কথা বলে আর ডাইভ দিয়ে বেড়ায়’', '<p>তোপটা ক্রিস্টিয়ানো রোনালদোই প্রথম দেগেছিলেন। আইসল্যান্ডের সঙ্গে ইউরোতে নিজেদের প্রথম ম্যাচে ১-১ গোলে ড্র করল পর্তুগাল। পর্তুগিজ অধিনায়ক ম্যাচের পর করলেন আইসল্যান্ডের রক্ষণাত্মক কৌশলের কড়া সমালোচনা। এবার জবাব এল উল্টো দিক থেকেও, একটু কড়া ভাষাতেই। সমালোচনার জবাব দিতে গিয়ে রোনালদোকে &lsquo;ছিঁচকাঁদুনে&rsquo; বলেছেন আইসল্যান্ড ডিফেন্ডার কারি আরনাসন।</p>\r\n', 'Article/07dcefe5280be3c9c027091572bbcb23-ronaldo-sore-looser.jpg', '', '', '2016-06-15 05:55:57', '2016-06-15 05:55:57'),
(17, 1, 1, 'Death sentence for 6 confirmed', '<p>The High Court yesterday confirmed the death penalty of six persons, including a central Jubo Dal leader, for killing Awami League lawmaker Ahsanullah Master, a decade after a trial court sentenced 22 people to death and six others to life term for the 2004 murder.</p>\r\n', 'Article/default.jpg', '', '', '2016-06-15 15:41:37', '2016-06-15 15:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `forums`
--

CREATE TABLE `forums` (
  `id` int(10) UNSIGNED NOT NULL,
  `forum_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `forum_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `forums`
--

INSERT INTO `forums` (`id`, `forum_title`, `forum_detail`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'News', '<p>News</p>\r\n', 0, '2016-06-13 22:24:07', '2016-06-13 22:24:07'),
(2, 'Sports', '<p>sports</p>\r\n', 0, '2016-06-13 22:35:11', '2016-06-13 22:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_06_043125_create_profiles_table', 1),
('2016_06_07_112220_create_forums_table', 1),
('2016_06_12_064221_create_articles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `personal_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `office_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_address` text COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone`, `office_phone`, `home_phone`, `current_address`, `permanent_address`, `birthday`, `gender`, `profile_picture`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rezaul', 'Islam', '1234``', '', '', '', '', '0000-00-00', '', 'default.jpg', '2016-06-14 01:51:19', '2016-06-14 01:51:19'),
(2, 2, 'Kamal', 'Hossain', '1224324', '', '', '', '', '0000-00-00', '', 'default.jpg', '2016-06-14 02:04:54', '2016-06-14 02:04:54'),
(4, 4, 'Abul ', 'Hossain', '123', '345', '234', 'DDD', 'sgdsgd', '0000-00-00', 'male', 'Pimage/25march26.jpg', '2016-06-14 09:57:18', '2016-06-14 09:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hasan', 'hasan@gmail.com', '$2y$10$lR0l9jYb7drjkypX/Y1GlOWQNwO9ROYPtEsaBTDxRs7ychrjELJhO', 0, 'TMkLzav72icDAQ6rQFHh0ClJzRHz4hN7Z5U9vHisYUunsVT7OtVSNKqKUaAr', '2016-06-13 22:16:40', '2016-06-15 15:44:34'),
(2, 'Kamal', 'kamal@gmail.com', '$2y$10$VH90OQfJpxilT7PpIPdivO3p35jiZYYnIzjJAhML1fLr2f9nNJk7G', 0, 'P7xuTpHeQZRPaLDbPZ9uwm24lkwQp3jFsfmY4MSs3usBpqdZD2NcY78Oy77M', '2016-06-14 00:24:39', '2016-06-14 03:51:18'),
(3, 'Babul', 'babul@gmail.com', '$2y$10$9u1SLaSwy0NCRNc/.IZpnOvxHq2RHO96MtNz7yefm3C2Alswjhdli', 0, 'c7FFOyJHIbvjw4vNm70M2dlMRw3x4B2EAaWftnR8irxYINHsXq2tTpo5fvNt', '2016-06-14 03:51:44', '2016-06-14 03:55:51'),
(4, 'Abul', 'abul@gmail.com', '$2y$10$dNfUX2kz0ByqHbQbuzD6/.32nNZYPe4Dc1v6KK.IXtMF7sAK4cKI.', 0, 'claFIdpXweJn4MFz4tsV1KzjQGNTwSE9HFjpDfodlO5aOuHDnoZlSXXUa7Ne', '2016-06-14 03:56:21', '2016-06-14 03:57:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_forum_id_foreign` (`forum_id`),
  ADD KEY `articles_user_id_foreign` (`user_id`);

--
-- Indexes for table `forums`
--
ALTER TABLE `forums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profiles_user_id_unique` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `forums`
--
ALTER TABLE `forums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_forum_id_foreign` FOREIGN KEY (`forum_id`) REFERENCES `forums` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
