<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('contact', function () {
    return view('pages.contact');
});

//Route::get('articles', 'ArticlesController@index');
//Route::get('articles/create', 'ArticlesController@create');
//Route::post('articles', 'ArticlesController@store');

Route::resource('profiles', 'ProfileController');
Route::resource ('forums', 'ForumController');
Route::resource ('/allforum', 'ForumController@allforum');
Route::resource ('articles', 'ArticleController');
Route::resource('/mypost', 'ArticleController@mypost');
Route::resource('/forumpost', 'ArticleController@forumpost');
Route::resource('/myprofile', 'ProfileController@myprofile');

//Route::get('profiles/profile','ProfileController@profile');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


Route::auth();

Route::get('/home', 'ArticleController@index');

Route::get('/', 'ArticleController@index');
