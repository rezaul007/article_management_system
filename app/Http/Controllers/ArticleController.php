<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Forum;

use App\Http\Requests;
//use Request;
use Auth;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
   // use ImageController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth',['only' => ['create', 'edit', 'destroy']]);
    }

    public function index()
    {
//        $articles = Article::all();
        $articles = Article::orderBy('created_at', 'desc')->get();
//        ->get();
//        $articles = Article::orderBy('created_at', 'desc');
        //return $articles;
        //dd($articles);

//        $id = Auth::user()->id;
//        $articles = Article::orderBy('created_at', 'desc')->get()->where('user_id',$id) ;
        
        return view('articles.index', compact('articles'));
        //return view('welcome',compact('articles'));
    }

    public function mypost()
    {
//        $articles = Article::all();
        $id = Auth::user()->id;
        $articles = Article::orderBy('created_at', 'desc')->get()->where('user_id',$id) ;
        return view('articles.index', compact('articles'));
        //return view('welcome',compact('articles'));
    }    
    
    public function forumpost()
    {
//        $articles = Article::all();
        $id=2;
        $articles = Article::orderBy('created_at', 'desc')->get()->where('forum_id',$id) ;

        return view('articles.index', compact('articles'));
        //return view('forums.show', compact('articles'));
        //return view('welcome',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $forums = Forum::lists('forum_title', 'id');
        return view('articles.create',compact('forums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article=new Article();
        $article->article_title=$request->article_title;
        $article->article_detail=$request->article_detail;
        
        $img=$request->file('image');
        $imgName=$img->getClientOriginalName();
        $article->img_path='Article/'.$imgName;
        $article->forum_id=$request->forum_id;
        $article->user_id=$request->user_id;
        $article->save();


        $destinationPath=public_path('/Article');
        $img->move($destinationPath, $imgName);
        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $articles)
    {
        //dd($articles);
        return view('articles.show',compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $articles)
    {
        return view('articles.edit',compact('articles' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ArticleRequest $request, Article $articles)
    {
        //dd($forum);
        $articles->update($request->all());


        return redirect('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $articles)
    {
        $articles->delete();
        return redirect('articles');
    }


    public function ImageUpload($image)
    {
        $extension =$image->getClientOriginalExtension();//get image extension only
        $extensionArray = array("jpg", "jpeg", "png", "gif");

        $path="image";
        if(in_array($extension, $extensionArray)){
            $imageOriginalName=$image->getClientOriginalName();//get image full name
            $basename = substr($imageOriginalName, 0 , strrpos($imageOriginalName, "."));//get image name without extension
            $imageName=$basename.date("YmdHis").'.'.$extension;//make new name

            $imageMoved=$image->move($path, $imageName);
            if($imageMoved)
            {
                $imagePath=$path.'/'.$imageName;
                return $imagePath;
            }

        }
        else{
            return "Please insert image";
        }
    }
    
    
}
