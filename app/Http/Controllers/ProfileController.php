<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use App\Http\Requests;
//use Request;
use Auth;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'get all articles';
        $profiles = Profile::all();
        //return $articles;
        return view('profiles.index',compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $input = Request::all();
//        Profile::create($input);
//        return redirect('profiles');

        $profile=new Profile();
        $profile->first_name=$request->first_name;
        $profile->last_name=$request->last_name;
        $profile->personal_phone=$request->personal_phone;
        $profile->home_phone=$request->home_phone;
        $profile->office_phone=$request->office_phone;
        $profile->current_address=$request->current_address;
        $profile->permanent_address=$request->permanent_address;
        $profile->birthday=$request->birthday;
        $profile->gender=$request->gender;

        $img=$request->file('image');
        $imgName=$img->getClientOriginalName();
        $profile->profile_picture='Pimage/'.$imgName;
//        $profile->forum_id=$request->forum_id;
        $profile->user_id=$request->user_id;
        $profile->save();


        $destinationPath=public_path('/Pimage');
        $img->move($destinationPath, $imgName);
        return redirect('profiles');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profiles)
    {
        //$profile1=User::with('profile')->where('id',1)->get()->toArray();
        //return view('profiles.show',compact('profile1'));
        //dd($profile1);
        //$profile1=User::find(1)->profile;
//        dd($profile1);
//        $id = Auth::user()->id;
//        $profile  = new Profile();
//        $profiles = $profile-> ;
        //dd($profiles);
        return view('profiles.show',compact('profiles'));
    }

    public function myprofile(){
//        $id = Auth::user()->id;
//        $profiles = Profile::('created_at', 'desc')->get()->where('user_id',$id) ;
//        dd($profiles);
        //dd($profiles);
        
        return view('profiles.profile',array('user'=>Auth::user()));
//array('user'=>Auth::user()
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profiles)
    {
        return view('profiles.edit',compact('profiles' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ProfileRequest $request, Profile $profiles)
    {
        //dd($forum);
        $profiles->update($request->all());


        return redirect('profiles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profiles)
    {
        $profiles->delete();
        return redirect('profiles');
    }

    public function ImageUpload($image)
    {
        $extension = $image->getClientOriginalExtension();//get image extension only
        $extensionArray = array("jpg", "jpeg", "png", "gif");

        $path = "image";
        if (in_array($extension, $extensionArray)) {
            $imageOriginalName = $image->getClientOriginalName();//get image full name
            $basename = substr($imageOriginalName, 0, strrpos($imageOriginalName, "."));//get image name without extension
            $imageName = $basename . date("YmdHis") . '.' . $extension;//make new name

            $imageMoved = $image->move($path, $imageName);
            if ($imageMoved) {
                $imagePath = $path . '/' . $imageName;
                return $imagePath;
            }

        } else {
            return "Please insert image";
        }
    }
}
