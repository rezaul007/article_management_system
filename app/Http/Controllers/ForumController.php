<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\Http\Requests;
//use Illuminate\Support\Facades\Auth;
//use Request;

class ForumController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth',['except' => 'index']);
        $this->middleware('auth',['only' => ['create', 'edit', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = Forum::all();
        
        return view('forums.index',compact('forums'));
    }

    public function allforum()
    {
        $forums = Forum::all();
        return view('forums.allforum',compact('forums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forums.create');
//        return view('yourview.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Forum::create($input);
        return redirect('forums');
        //return $input;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Forum $forums)
    {
        //dd($forums);
        return view('forums.show',compact('forums'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forums)
    {
        return view('forums.edit',compact('forums' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ForumRequest $request, Forum $forums)
    {
        //dd($forum);
        $forums->update($request->all());


        return redirect('forums');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Forum $forums)
    {
        $forums->delete();
        return redirect('forums');
    }
    
    
}
