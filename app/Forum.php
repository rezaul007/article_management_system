<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    

    protected $fillable = [
        'forum_title',
        'forum_detail',
    ];
    public function articles()
    {
        return $this->hasMany('App\Article');
    }


}



