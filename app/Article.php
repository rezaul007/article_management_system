<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $guarded=[];
    protected $fillable = [
      'article_title',
        'article_detail',
        'forum_id',
        'user_id'
        //'extension'

        ];

    public function forum()
    {
        return $this->belongsTo('App\Forum');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
